import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)

    data = response.json()
    photo_url = {"picture_url": data["photos"][0]["url"]}

    return photo_url


def get_weather(city, state):
    params = {"appid": OPEN_WEATHER_API_KEY, "q": city + "," + state + ",US"}
    url = "http://api.openweathermap.org/geo/1.0/direct?"

    response = requests.get(url, params=params)
    response = response.json()
    try:
        lat = response[0]["lat"]
        lon = response[0]["lon"]
    except (IndexError, KeyError):
        return {"message": f"No data on {city}, {state}, US."}

    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather?"

    response = requests.get(url, params=params)
    response = response.json()

    weather = response["weather"][0]["main"]
    temperature = response["main"]["temp"]

    return {"weather": weather, "temperature": temperature}
